#!/usr/bin/python

from functions import *

runcount = 1

while True:
    print "Checking " + time.strftime("%a, %d %b %Y %H:%M:%S", time.gmtime())

    cur.execute("SELECT * FROM user WHERE active = 1;")
    results = cur.fetchall();

    for row in results:
        check_person(row)

    print "Iteration ", runcount
    runcount = runcount + 1
    time.sleep(5)


# notes
# -----
#
# add flag for new device added - use to know if full list should be rechecked
#
# save time that full list was last checked. only pull new list every N minutes to minimize db access (ryo cache kinda)
#
# investigate bug where db was notupdating until a new device came online.
#

