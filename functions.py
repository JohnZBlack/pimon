import MySQLdb
import datetime
import bluetooth
import os
import time

from conf import *

db = MySQLdb.connect(host=str(database.host),
                     user=str(database.user),
                     passwd=str(database.password),
                     db=str(database.name))

cur = db.cursor()

def check_person(person):
    print "Checking " + person[2]
    device = person[3]
    result = bluetooth.lookup_name(device, timeout=5)

    if result is not None:
        time_since_last_seen = datetime.datetime.now() - person[6]
        minutes_since_last_seen = (time_since_last_seen.total_seconds() / 60)
        if (person[4] == "out"):
	    #print "time since: "
	    #print time_since_last_seen
            #print "seconds since: "
	    #print time_since_last_seen.total_seconds()
            print "minutes since: "
            print minutes_since_last_seen

            if(minutes_since_last_seen >= 180):
                print person[2] + " was lost but now is found"
                cmd = "mpg321 -g 30 " + pimon.audioFolder + person[5]
                os.system(cmd)

                persist(person, 'oi')
            else:
                persist(person, 'oi')

        else:
            persist(person, 'ii')

    else:
        if(person[4] == "in"):
            persist(person, 'io')

    return

def persist(person, updateType):

    if(updateType == 'oi'):
        print "OI " + person[2]
        status = 'in'
        address=person[3]
        cur.execute ("UPDATE user SET status='%s', last_seen= NOW() WHERE bluetooth_address='%s';" % (status, address))
    elif(updateType == 'io'):
        print "IO " + person[2]
        status = 'out'
        address=person[3]
        cur.execute ("UPDATE user SET status='%s' WHERE bluetooth_address='%s';" % (status, address))
    elif(updateType == 'ii'):
        print "II " + person[2]
        address=person[3]
        cur.execute ("UPDATE user SET last_seen= NOW() WHERE bluetooth_address='%s';" % (address))

    db.commit()
    return True

def scanForDevices():
    print("Searching for discoverable devices...")
    devices = bluetooth.discover_devices(lookup_names = True)

    print("Found %d devices" % len(devices))

    for address, name in devices:
        cur.execute("SELECT * FROM user WHERE bluetooth_address ='%s';" % (address))
        result=cur.fetchone()

        if(result):
            if(result[7] == 1):
                print("Known device located:  %s - %s. Assigned to %s %s" % (address, name, result[2], result[1]))
            else:
                print("Known but unassigned device:  %s - %s" % (address, name))
        else:
            print("New device found! %s - %s - adding to DB..." % (address, name))
            query = "INSERT INTO user (bluetooth_address, audio_file, active, device_name) VALUES (%s, 'default.mp3', 0, %s);"

            cur.execute(query, (address, name))

            db.commit()

    return True








